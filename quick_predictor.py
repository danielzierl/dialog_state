from main import argument_parsing, load_run_config
from models_.simple_model.model import Model as SimpleModel
from models_.part_context_model.model import Model as PartContextModel
from models_.d3st_model.model import Model as D3STModel
from models_.pipeline import get_local_config_curry_data_load
from t5s import T5

from modules_.logging_ import setup_logging, get_logger

model_name = "d3st_model"


def main():
    # get arguments from command line
    args = argument_parsing()
    # setup logging
    setup_logging(args.debug_level)
    # load the project wide config
    global_config = load_run_config(args.config_location)
    global_config["models"][model_name]["LOAD_CHECKPOINT"] = True
    model = D3STModel(global_config, model_name,
                      get_local_config=get_local_config_curry_data_load(
                          global_config["DATA_LOAD_LOC"], global_config["DATA_SAVE_PREPROCESS"], model_name=model_name),
                      dataset=None)
    while True:
        text = input("Text: ")
        out = model.predict([text])
        print(out)


if __name__ == "__main__":
    main()
