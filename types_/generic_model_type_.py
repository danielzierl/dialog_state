from dataclasses import dataclass
from typing import Callable
from types_.generic_dataset_type_ import GenericDataset
from types_.global_config_type import GlobalConfigType


@dataclass
class GenericModel:
    run_pipeline: Callable
    dataset: GenericDataset
    name: str
    global_config: GlobalConfigType
    model: object
    train: Callable
    predict_json: Callable
    evaluate: Callable
