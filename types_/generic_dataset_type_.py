from dataclasses import dataclass
from datasets import DatasetDict


@dataclass
class GenericDataset:
    data_train: DatasetDict
    data_dev: DatasetDict
