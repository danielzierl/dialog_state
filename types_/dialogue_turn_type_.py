from dataclasses import dataclass

from typing import TypedDict


@dataclass
class DialogueTurn(TypedDict):
    user: str
    user_state: str
    system: str | None
    prev_state: str | None
    line_nr: int
    turn_id: int
    dialog_id: str
    dialogue_history: str


# the difference of d3st will be that the user state is now mapped randomly

@dataclass
class DialogueTurnD3ST(DialogueTurn):
    user_state_non_maped: str
    slot_map: dict
    descriptions: dict
