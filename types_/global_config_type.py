from dataclasses import dataclass
from typing import TypedDict


@dataclass
class ModelParams(TypedDict):
    MODEL_CHECKPOINT: str
    INPUT_SIZE: int
    OUTPUT_SIZE: int
    EPOCHS: int
    STEPS_PER_EPOCH: int
    BATCH_SIZE_PREDICT: int
    BATCH_SIZE_TRAIN: int
    PEFT_RANK: int
    LR: float
    PRETRAINED: str
    SAVE_CHECKPOINT_EVERY: int
    LOAD_CHECKPOINT: bool
    BASE_MODEL: str


@dataclass
class InnerModel(TypedDict):
    name: str
    params: ModelParams


@dataclass
class GlobalConfigType(TypedDict):
    models: dict[str, InnerModel]
    TRY_NUM_MAX: int
    DATA_LOAD_LOC: str
    DATA_SAVE_PREPROCESS: str
    DATA_PREDICT_LOC: str
    DATA_EVAL_LOC: str
    CACHE_DIR: str
