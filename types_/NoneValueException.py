class NoneValueExeption(Exception):
    def __init__(self, message_problem):
        message = f"NoneException: the value {message_problem} is None"
        super.__init__(message)
