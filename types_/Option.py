from .NoneValueException import NoneValueExeption


class Option:
    def __init__(self, value, log_name="no_name_supplied"):
        self.value = value
        self.log_name = log_name

    def is_none(self):
        return self.value is None

    def is_some(self):
        return self.valuse is not None

    def unwrap(self):
        if self.is_some:
            return self.value
        else:
            raise NoneValueExeption(self.log_name)
