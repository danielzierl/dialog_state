from tqdm import tqdm
import os
from modules_.logging_ import get_logger


def convert_to_tsv(filename_out, data_grouped_by_dialogues, tsv_writer_func, silent=False, do_append=False):
    n = 0
    if not os.path.exists(filename_out):
        get_logger().info("Creating {} dirs".format(filename_out))
        os.makedirs(os.path.dirname(filename_out), exist_ok=True)

    with open(filename_out, "w" if not do_append else "a", encoding="utf-8") as fw:
        for dialogue_data in tqdm(data_grouped_by_dialogues.values()):
            dialgue_input_output_tuple_list = tsv_writer_func(dialogue_data)
            [print(input, output, sep="\t", file=fw)
             for input, output in dialgue_input_output_tuple_list]
            n += 1
    if not silent:
        print("Written", n, "lines")
