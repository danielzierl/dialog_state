import logging
import colorlog


_logger: logging.Logger | None = None
sep_len = 50


def setup_logging(debug_level: str):
    global _logger
    if _logger is not None:
        return
    _logger = logging.getLogger(__name__)
    color_formatter = colorlog.ColoredFormatter(
        f" %(log_color)s\n {'-'*sep_len}\n %(levelname)s - %(asctime)s - %(name)s :%(message)s\n {'-'*sep_len} ",
        datefmt=None,
        reset=True,
        log_colors={
            'DEBUG': 'cyan,bg_black',
            'INFO': 'green,bg_black',
            'WARNING': 'yellow,bg_black',
            'ERROR': 'purple,bg_black',
            'CRITICAL': 'red,bg_black',
        },
        secondary_log_colors={},
        style='%'
    )
    console_handler = logging.StreamHandler()
    console_handler.setFormatter(color_formatter)
    file_handler = logging.FileHandler("log.txt")
    file_handler.setFormatter(color_formatter)
    _logger.addHandler(console_handler)
    _logger.addHandler(file_handler)
    _logger.setLevel(getattr(logging, debug_level))
    _logger.info("LOGGING MODE: %s", debug_level)


def get_logger():
    if _logger is None:
        print("please setup logger")
        exit(1)
    return _logger
