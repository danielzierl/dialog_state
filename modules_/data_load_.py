# import torch
from types_.dialogue_turn_type_ import DialogueTurn
import os
import glob
import json
import re
from modules_.logging_ import get_logger


def curl_data_if_nonexistent(save_location: str, force_reload=False):
    if os.path.exists(save_location) and os.listdir(save_location) and not force_reload:
        get_logger().info("skipping curl data")
        return
    get_logger().info("curling data")
    os.system(f"mkdir -p {save_location}")
    os.system(
        f"curl https://storage.googleapis.com/gresearch/dstc11/train.tts-verbatim.2022-07-27.txt -o {save_location}/tts_verbatim_train.txt")
    os.system(
        f"curl https://storage.googleapis.com/gresearch/dstc11/dev-dstc11.2022-07-27.txt -o {save_location}/tts_verbatim_dev.txt")
    os.system(
        f"curl https://storage.googleapis.com/gresearch/dstc11/test-dstc11.2022-09-21.txt -o {save_location}/tts_verbatim_test.txt")
    os.system(
        f"gsutil cp -r gs://t5-data/vocabs/cc_all.32000/ ./{save_location}/")

    # load audio files
    zip_loc = os.path.join(save_location, "zips/")
    if not os.path.exists(zip_loc):
        get_logger().info("Creating zip dir")
        os.makedirs(os.path.dirname(zip_loc), exist_ok=True)

    os.system(
        f"curl https://storage.googleapis.com/gresearch/dstc11/train.tts-verbatim.2022-07-27.zip -o {save_location}/zips/tts_verbatim_train_audio.zip"
    )
    os.system(
        f"curl https://storage.googleapis.com/gresearch/dstc11/dev-dstc11.tts-verbatim.2022-07-27.zip -o {save_location}/zips/tts_verbatim_dev_audio.zip"
    )
    os.system(
        f"curl https://storage.googleapis.com/gresearch/dstc11/dev-dstc11.human-verbatim.2022-09-29.zip -o {save_location}/zips/human_verbatim_dev_audio.zip"
    )
    os.system(
        f"curl https://storage.googleapis.com/gresearch/dstc11/dev-dstc11.human-paraphrased.2022-11-02.zip -o {save_location}/zips/human_paraphrased_dev_audio.zip"
    )
    unzip_all_voice(save_location)


def unzip_all_voice(save_location: str):
    os.system(
        f"unzip {save_location}/zips/tts_verbatim_train_audio.zip -d {save_location}/"
    )
    os.system(
        f"unzip {save_location}/zips/tts_verbatim_dev_audio.zip -d {save_location}/"
    )
    os.system(
        f"unzip {save_location}/zips/human_verbatim_dev_audio.zip -d {save_location}/"
    )
    os.system(
        f"unzip {save_location}/zips/human_paraphrased_dev_audio.zip -d {save_location}/"
    )


class TTSVerbatimMappingDataset():
    def __init__(self, data_dir: str):
        get_logger().info("loading data" + data_dir)
        filename_train = glob.glob(os.path.join(
            data_dir, "*_train.*"), recursive=False)[0]
        self.data_train = _load_data_to_dict(filename_train)
        filename_dev = glob.glob(os.path.join(
            data_dir, "*_dev.*"), recursive=False)[0]
        self.data_dev = _load_data_to_dict(filename_dev)


def _load_data_to_dict(file_name):
    with open(file_name) as f:
        data: list[DialogueTurn] = []
        dialog_turn = 0
        prev_dialog_id = None
        two_row_buffer = []
        for row in f:

            if "END_OF_DIALOG" in row:
                continue
            row = row.strip()
            pattern_dialog_id = r'dialog_id:\s*(\w+.\w+)\s'
            dialog_id = re.findall(pattern_dialog_id, row)
            if prev_dialog_id != dialog_id[0]:
                dialog_turn = 0
            prev_dialog_id = dialog_id[0]
            pattern_line_nr = r'line_nr:\s*(\d+)'
            pattern_turn_id = r'turn_id:\s*(\d+)'
            pattern_user_utterance = r'user:\s*(.*)state:'
            pattern_user_state_utterance = r'state:\s*(.*)'
            pattern_agent_utterance = r'agent:\s*(.*)state:'

            line_nr = re.findall(pattern_line_nr, row)
            turn_id = re.findall(pattern_turn_id, row)

            if dialog_turn == 0:

                d: DialogueTurn = {
                    "user": re.findall(pattern_user_utterance, row)[0].strip(),
                    "user_state": re.findall(pattern_user_state_utterance, row)[0].strip(),
                    "system": '',
                    "prev_state": '',
                    "line_nr": int(line_nr[0]),
                    "turn_id": int(turn_id[0]),
                    "dialog_id": dialog_id[0],
                    "dialogue_history": ""
                }
                data.append(d)
            else:
                # not a new dialog
                two_row_buffer.append(row)
                if (len(two_row_buffer) == 1):
                    continue
                else:
                    d: DialogueTurn = {
                        "user": re.findall(pattern_user_utterance, two_row_buffer[1])[0].strip(),
                        "user_state": re.findall(pattern_user_state_utterance, two_row_buffer[1])[0].strip(),
                        "prev_state": data[-1]["user_state"],
                        "system": re.findall(pattern_agent_utterance, two_row_buffer[0])[0],
                        "line_nr": int(line_nr[0]),
                        "turn_id": int(dialog_turn),
                        "dialog_id": dialog_id[0],
                        "dialogue_history": f"{data[-1]['dialogue_history']} {data[-1]['system']} {data[-1]['user']}"
                    }
                    data.append(d)
                    two_row_buffer = []
            dialog_turn += 1

        return data
