from typing import Callable
from modules_.data_process_ import convert_to_tsv
from util_.minfuncs import load_json_to_dict
from modules_.logging_ import get_logger
from types_.global_config_type import ModelParams
from types_.generic_model_type_ import GenericModel
from types_.dialogue_turn_type_ import DialogueTurn


class PredictState:
    file_alr_opened = False


def predict_json(
        from_file_json: str,
        to_file_tsv: str,
        model: GenericModel,
        num_batches=None,
        get_text_repr_func=None,
        tsv_writer_func: Callable = None,
        predict_output_transform_func: Callable = None
):
    predict_state = PredictState()
    params: ModelParams = model.global_config["models"][model.name]["params"]

    if (params is None):
        get_logger().error("no params specified")
    if (tsv_writer_func is None):
        get_logger().error("no tsv writer function specified")
    if (get_text_repr_func is None):
        get_logger().error("no predict function specified")

    batch_counter = 0
    ground_truth = load_json_to_dict(from_file_json)
    dialogue_batch = []
    for index, (dialog_name, dialog_turn_arr) in enumerate(ground_truth.items(), 1):
        dialogue_batch.append(dialog_turn_arr)
        # take a whole batch of dialogues
        if index % params["BATCH_SIZE_PREDICT"] == 0 or index == len(ground_truth):
            print("predicting batch", batch_counter)
            if num_batches is not None and num_batches <= batch_counter:
                return
            predict_state = predict_for_dialogue_batch(
                dialogue_batch,
                model,
                to_file_tsv,
                predict_state,
                tsv_writer_func=tsv_writer_func,
                predict_turn_get_text_repr_func=get_text_repr_func,
                predict_output_transform_func=predict_output_transform_func
            )
            batch_counter += 1
            dialogue_batch = []


def predict_for_dialogue_batch(
        dialogue_batch: list,
        model: GenericModel,
        to_file_tsv: str,
        predict_state: PredictState,
        tsv_writer_func: Callable,
        predict_turn_get_text_repr_func: Callable,
        predict_output_transform_func: Callable
):
    # used as a buffer for predition
    dialogue_turns_batch = {}
    active_dialogues_indices = {}

    predictions_dialogue_scope = {}
    prediction_buffer = [""] * len(dialogue_batch)
    # prev_state_buffer = [""] * len(dialogue_batch)

    # first we take each dialogue and select N-th turn
    for turn_index in range(max(len(d) for d in dialogue_batch)):
        print("turn", turn_index)
        for index_in_dialogue_batch, dialogue in enumerate(dialogue_batch):
            if len(dialogue) > turn_index:
                dialogue_turns_batch[index_in_dialogue_batch] = dialogue[turn_index]
                active_dialogues_indices[index_in_dialogue_batch] = True

        # this should leave me with a dict of active dialogue turns
        # in every of the dialogues in the batch
        # dialogues are in json representation
        model_input_batch = []
        for dialogue_turn, prev_state in \
                zip(dialogue_turns_batch.values(), prediction_buffer):
            dialogue_turn: DialogueTurn
            model_input, out = predict_turn_get_text_repr_func(dialogue_turn)
            model_input = preprocess_dialogue_row(model_input)
            model_input_batch.append(model_input)

        # we are ready to predict
        prediction_batch = model.predict(model_input_batch)
        print(prediction_batch)

        prediction_batch = [postprocess_predicted_state(
            prediction) for prediction in prediction_batch]
        print(prediction_batch)

        # if we have a model specific prediction output transforming
        # function we should apply it
        if predict_output_transform_func is not None:
            for i, (pred, dialogue_turn) in \
                    enumerate(zip(prediction_batch,
                              dialogue_turns_batch.values())):
                prediction_batch[i] = predict_output_transform_func(
                    pred, dialogue_turn)
        print(prediction_batch)

        # save the value for next turn
        # add prediction to correct dialogue
        for dialogue_index, prediction, prev_state in \
                zip(
                    active_dialogues_indices,
                    prediction_batch,
                    prediction_buffer):

            # set new arr if doesnt exist
            if dialogue_index not in predictions_dialogue_scope:
                predictions_dialogue_scope[dialogue_index] = []

            # lets take the correct dialogue turn
            _dial_turn = dialogue_batch[dialogue_index][turn_index]
            _dial_turn["user_state"] = prediction

            _dial_turn["prev_state"] = prev_state
            predictions_dialogue_scope[dialogue_index].append(_dial_turn)

        # fix for next prediction from random indices to slot names
        # prediction_batch = [remap_fullslots_from_slot_map(prediction, dialogue_turn["slot_map"]) for prediction, dialogue_turn in zip(prediction_batch, dialogue_turns_batch.values())]
        prediction_buffer = prediction_batch
        active_dialogues_indices = {}
        # prev_state_buffer = [prev_state + " " + newInputs for prev_state, newInputs in zip(prev_state_buffer, dialogue_rows_batch_no_prev)]
    return prediction_and_dialogue_flush_to_tsv(
        to_file_tsv,
        predictions_dialogue_scope,
        predict_state,
        tsv_writer_func)


def prediction_and_dialogue_flush_to_tsv(
        tsv_name: str,
        data: dict,
        predict_state: PredictState,
        tsv_writer_function: Callable) -> PredictState:
    convert_to_tsv(
        tsv_name, data, tsv_writer_func=tsv_writer_function, silent=True, do_append=predict_state.file_alr_opened)
    predict_state.file_alr_opened = True
    return predict_state


def preprocess_dialogue_row(row: str):
    return row


def postprocess_predicted_state(state: str):
    state = state.replace("state:", "")
    state = state.replace("<state>", "")
    state = state.replace("</state>", "")
    return state
