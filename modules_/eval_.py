import re
# from symspellpy import SymSpell, Verbosity
# import tensorflow as tf
# from predict_ import predict_json
# import wandb
import os
import time
import json
from tqdm import tqdm
from prettytable import PrettyTable
from util_.minfuncs import clean_and_parse_fullslots
from modules_.logging_ import get_logger


# class CustomEvaluationCallback(tf.keras.callbacks.Callback):
#     def __init__(self, model):
#         self._model = model
#
#     def on_epoch_end(self, epoch, logs=None):
#         print("\nprediction for evaluation\n")
#         predict_json("data_tsv/tts_verbatim_dev.json",
#                      "data_tsv/tts_verbatim_dev.pred.tsv", self._model, 1)
#         print("\nevaluating")
#         results = evaluate("data_tsv/tts_verbatim_dev.pred.tsv",
#                            "data_tsv/tts_verbatim_dev.tsv")
#         wandb.log(results)
#

def main():
    evaluate("data_tsv/tts_verbatim_dev.pred.tsv",
             "data_tsv/tts_verbatim_dev.tsv", "data_tsv/tts_verbatim_dev.json")


# pred and gold are tsv, json is neccessary to load metadata
def evaluate(pred_file, gold_file, pred_file_json=None, eval_folder="eval"):
    get_logger().info("evaluating")
    sl_both_wrong_buffer_arr = []
    sl_not_found_buffer_arr = []
    sl_extra_found_buffer_arr = []
    data_pred = load_data_from_tsv_to_json(open(pred_file, 'r'))
    data_gold = load_data_from_tsv_to_json(open(gold_file, 'r'))
    data_pred_json = json.load(open(pred_file_json, 'r'))
    data_pred_arr_json = []
    [data_pred_arr_json.append(turn)
     for dialog in data_pred_json.values() for turn in dialog]
    joint_acc = evaluate_JGD(data_pred, data_gold, eval_folder, data_pred_arr_json,
                             sl_both_wrong_buffer_arr, sl_not_found_buffer_arr, sl_extra_found_buffer_arr)
    ser = evaluate_SER(data_pred, data_gold)
    print('joint accuracy: {}'.format(joint_acc))
    print('ser: {}'.format(ser))
    prepend_table_with_info(eval_folder, joint_acc, ser, len(
        data_pred), sl_both_wrong_buffer_arr, sl_not_found_buffer_arr, sl_extra_found_buffer_arr)
    return {
        'joint_acc': joint_acc,
        'ser': ser,
    }


def load_data_from_tsv_to_json(file):
    out = []
    for line in file:
        line = line.strip()
        line = line.split("\t")

        pattern_state = r'state:\s*(.*)'
        state = re.findall(pattern_state, line[1])
        if not len(state) > 0:
            pattern_state = r'<state>\s*(.*)</state>'
            state = re.findall(pattern_state, line[1])[0]

        out.append(state)
    return out


def cleaning(pred: str, target):
    # pred = re.sub(r"(\d+)", lambda x: num2words.num2words(int(x.group(0))), pred)
    # target = re.sub(r"(\d+)", lambda x: num2words.num2words(int(x.group(0))), target)
    pred = pred.replace("'", "")

    target = target.replace("'", "")
    pred = pred.replace("state:", "")
    target = target.replace("state:", "")
    pred = pred.split(";")
    pred = [e.strip() for e in pred]

    target = target.split(";")
    target = [e.strip() for e in target]
    return [pre.lower() for pre in pred if pre], [targ.lower() for targ in target if targ]
    # return [correct_spelling(pre.lower()) for pre in pred if pre], [correct_spelling(targ.lower()) for targ in target if targ]


def evaluate_JGD(
        data_pred,
        data_gold,
        save_name_dir,
        pred_json,
        sl_both_wrong_buffer_arr=None,
        sl_not_found_buffer_arr=None,
        sl_extra_found_buffer_arr=None):

    num_turns = 0
    joint_acc = 0

    if not os.path.exists(save_name_dir):
        os.makedirs(save_name_dir)
    with open(save_name_dir + "/results.txt", 'w') as file_pretty:
        for pred, target, json_d in zip(tqdm(data_pred), data_gold, pred_json):
            # pred = remap_fullslots_from_slot_map(pred, json_d["slot_map"])
            # target = remap_fullslots_from_slot_map(target, json_d["slot_map"])

            pred, target = cleaning(pred, target)
            try:
                table = PrettyTable(["Type", "Value"])
                table.add_row(["System", json_d["system"]])
                table.add_row(["User", json_d["user"]])
                table.add_row(["Predicted", pred])
                table.add_row(["Target", target])
                a = set(pred).difference(set(target))
                table.add_row(["Incorrect in Prediction", a])
                b = set(target).difference(set(pred))
                table.add_row(["Not found in Prediction", b])
                a_name = set()
                b_name = set()
                [a_name.add(clean_and_parse_fullslots(_a)[0][0]) for _a in a]
                [b_name.add(clean_and_parse_fullslots(_b)[0][0]) for _b in b]
                pred_names = set()
                [pred_names.add(clean_and_parse_fullslots(_a)[0][0])
                 for _a in pred]
                target_names = set()
                [target_names.add(clean_and_parse_fullslots(_b)[0][0])
                 for _b in target]
                a_diff = pred_names.difference(target_names)
                b_diff = target_names.difference(pred_names)
                intr = a_name.intersection(b_name)
                sl_extra_found_buffer_arr.extend(a_diff)
                sl_not_found_buffer_arr.extend(b_diff)
                sl_both_wrong_buffer_arr.extend(intr)

                table.add_row(["Same", (set(target).intersection(set(pred)))])

                if set(target) == set(pred):
                    table.add_row(["Status", "Correct"])
                    joint_acc += 1
                else:
                    table.add_row(
                        ["Status", f"Wrong; {len(set(pred).intersection(set(target)))} \
                                out of {len(set(target))} correct"])

                num_turns += 1
                print(table, file=file_pretty)
            except Exception as e:
                print("something went wrong while creating eval table")

        joint_acc /= num_turns
        file_pretty.close()

        return joint_acc


def prepend_table_with_info(save_name_dir, joint_acc=0, ser=0, turns=0, sl_both_wrong_buffer_arr=None, sl_not_found_buffer_arr=None, sl_extra_found_buffer_arr=None):
    table = PrettyTable(
        ["Joint goal accuracy", "Slot error rate", "Dialog turns"])
    table.add_row([joint_acc, ser, turns])
    with open(save_name_dir + "/results.txt", 'r') as file_pretty:
        content = file_pretty.readlines()
    with open(save_name_dir + f"/results{time.time()}.txt", 'w') as file_pretty:
        print(table, file=file_pretty)

        print_sep(file_pretty)
        print("Wrong by slot value\n", file=file_pretty)
        print(text_bar_graph(count_duplicate_lines_from_list(
            sl_both_wrong_buffer_arr)), file=file_pretty)

        print_sep(file_pretty)
        print("Extra in predictions\n", file=file_pretty)
        print(text_bar_graph(count_duplicate_lines_from_list(
            sl_extra_found_buffer_arr)), file=file_pretty)

        print_sep(file_pretty)
        print("Missing from predictions\n", file=file_pretty)
        print(text_bar_graph(count_duplicate_lines_from_list(
            sl_not_found_buffer_arr)), file=file_pretty)

        file_pretty.writelines(content)


def print_sep(fs):
    print("-" * 50, file=fs)


def evaluate_SER(data_pred, data_gold):
    slot_count = 0
    intersect_slot_count = 0
    for pred, target in zip(data_pred, data_gold):
        pred, target = cleaning(pred, target)
        pred_slots = set(pred)
        target_slots = set(target)
        common_slots = pred_slots.intersection(target_slots)
        intersect_slot_count += len(common_slots)
        # slot_count += max(len(pred_slots), len(target_slots))
        slot_count += len(target_slots)
    return 1 - intersect_slot_count / slot_count


def ignore_none(pred_belief, target_belief):
    for pred in pred_belief:
        if 'catherine s' in pred:
            pred.replace('catherine s', 'catherines')

    clean_target_belief = target_belief
    clean_pred_belief = pred_belief
    for bs in target_belief:
        if 'not mentioned' in bs:
            continue
        clean_target_belief = (bs)

    for bs in pred_belief:
        if 'not mentioned' in bs:
            continue
        clean_pred_belief = (bs)

    return clean_pred_belief, clean_target_belief


# spell = SymSpell(max_dictionary_edit_distance=2, prefix_length=9)


# def correct_spelling(sentence):
#     sl_name, sl_value = clean_and_parse_fullslots(sentence)
#     if sentence is None:
#         return ""
#
#     # Split the sentence into words
#     words = sl_value[0].split()
#
#     # Find and correct misspelled words
#     # print(words)
#     corrected_value = []
#     for word in words:
#         try:
#             lookup = spell.lookup(word, Verbosity.CLOSEST,
#                                   include_unknown=True, max_edit_distance=5)
#             corrected_value.append(lookup[0].term)
#         except Exception:
#             corrected_value.append(word)
#
#     corrected_words = [w for w in corrected_value if w]
#
#     # Reconstruct the corrected sentence
#     corrected_sentence = ' '.join(corrected_words)
#
#     return sl_name[0] + "=" + corrected_sentence
#

def count_duplicate_lines_from_list(lines):
    # Count the occurrences of each line
    line_counts = {}
    for line in lines:
        line = line.strip()  # Remove leading/trailing whitespace
        line_counts[line] = line_counts.get(line, 0) + 1

    # Filter lines that occurred more than once
    duplicate_lines = {line: count for line,
                       count in line_counts.items() if count > 1}

    return duplicate_lines


def text_bar_graph(data):
    out = ""
    max_value = max(data.values())

    for key, value in data.items():
        # Calculate the number of asterisks proportional to the value
        bar_length = int(value / max_value * 30)
        bar = '*' * bar_length

        out += (f"{key:<25}: {bar} ({value})\n")
    return out


if __name__ == "__main__":
    main()
