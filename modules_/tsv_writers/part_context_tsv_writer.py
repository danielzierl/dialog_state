from types_.dialogue_turn_type_ import DialogueTurn

# these method specify how a dialogue passed as the inner json representation
# should be translated into raw text


# below is the implamentation for the part context writer that appends
# previous states to the dialogue turn


def tsv_writer(whole_dialogue_data):
    out = []

    for dialogue_turn in whole_dialogue_data:

        input, output = get_text_repr(dialogue_turn)
        out.append((input, output))

    return out


def get_text_repr(dialogue_turn: DialogueTurn, no_d3st=False):
    input = ""
    # add the previous states of dialogue
    input += f"{dialogue_turn['prev_state']}"

    # if system utterance, missing only on first turn
    if dialogue_turn["system"]:
        input += f" {dialogue_turn['system']}"

    # should always have user utterance
    input += f" {dialogue_turn['user']}"

    output = f"state: {dialogue_turn['user_state']}"
    # output = "state: test-state=dontcare"
    return input, output
