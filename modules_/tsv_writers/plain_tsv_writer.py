from types_.dialogue_turn_type_ import DialogueTurn

max_output_len = 1000

# these method specify how a dialogue passed as the inner json representation
# should be translated into raw text

# below is the implamentation for the basic writer, that concats the text


def tsv_writer(whole_dialogue_data):
    out = []

    for dialogue_turn in whole_dialogue_data:

        input, output = get_text_repr(dialogue_turn)
        out.append((input, output))

    return out


def get_text_repr(dialogue_turn: DialogueTurn):
    input = ""
    # add the previous whole history of dialogue
    input += f"{dialogue_turn['dialogue_history']}"

    # if system utterance, missing only on first turn
    if dialogue_turn["system"]:
        input += f" {dialogue_turn['system']}"

    # should always have user utterance
    input += f" {dialogue_turn['user']}"

    # just to be safe, clip the max output len
    input = input[:max_output_len]

    output = f"state: {dialogue_turn['user_state']}"
    # output = "i dont care"
    return input, output
