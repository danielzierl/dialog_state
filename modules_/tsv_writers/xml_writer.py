from types_.dialogue_turn_type_ import DialogueTurn, DialogueTurnD3ST

# these method specify how a dialogue passed as the inner json representation
# should be translated into raw text


# below is the implamentation for the part context writer that appends
# previous states to the dialogue turn


def tsv_writer(whole_dialogue_data, no_d3st=False):
    out = []

    for dialogue_turn in whole_dialogue_data:

        input, output = get_text_repr(dialogue_turn, no_d3st=no_d3st)
        out.append((input, output))

    return out


def get_text_repr(dialogue_turn: DialogueTurnD3ST, no_d3st=False):
    input = ""
    input = f"<slot_map>{dialogue_turn['slot_map']}</slot_map>"
    # add the previous states of dialogue
    input += f"<prev_state>{dialogue_turn['prev_state']}</prev_state>"

    # if system utterance, missing only on first turn
    if dialogue_turn["system"]:
        input += f" <agent>{dialogue_turn['system']}</agent>"

    # should always have user utterance
    input += f"<user>{dialogue_turn['user']}</user>"

    if no_d3st and 'user_state_non_mapped' in dialogue_turn:
        output = f"<state>{dialogue_turn['user_state_non_mapped']}</state>"
    else:
        output = f"<state>{dialogue_turn['user_state']}</state>"
    # output = "state: test-state=dontcare"
    return input, output
