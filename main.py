import yaml
import os
from types_.Option import Option
from types_.NoneValueException import NoneValueExeption
from modules_.logging_ import setup_logging, get_logger
import argparse
from models_.simple_model.model import Model as SimpleModel
from models_.part_context_model.model import Model as PartContextModel
from models_.d3st_model.model import Model as D3STModel
from models_.xml_d3st_context_model.model import Model as XMLD3STContextModel
from models_.mistral7B_model.model import Model as Mistral7BModel
from modules_.data_load_ import curl_data_if_nonexistent, \
    TTSVerbatimMappingDataset
from types_.global_config_type import GlobalConfigType
from types_.generic_dataset_type_ import GenericDataset
from models_.pipeline import get_local_config_curry_data_load
import wandb
# from modules_.data_load_ import unzip_all_voice

#
# ----------------------------------------------------------------
#


def main():

    # get arguments from command line
    args = argument_parsing()

    # setup logging
    setup_logging(args.debug_level)

    # load the project wide config
    global_config: GlobalConfigType | dict = load_run_config(
        args.config_location)
    get_logger().debug(global_config)

    wandb.init(
        project="dialog_state_tracking",
        config=global_config  # type: ignore
    )

    # curl necessary data
    curl_data_if_nonexistent(
        global_config["DATA_LOAD_LOC"], force_reload=args.force_reload)

    # create the dataset from curled data
    dataset = TTSVerbatimMappingDataset(global_config["DATA_LOAD_LOC"])

    # run the models
    run_models(global_config, dataset, args)  # type: ignore

#
# ----------------------------------------------------------------
#


def run_models(
        global_config: GlobalConfigType,
        dataset: GenericDataset,
        args: argparse.Namespace) -> None:
    models = get_model_dict(global_config, dataset)
    if not models:
        get_logger().warning("No models to run")
        return
    for model_name, model_conf in global_config["models"].items():
        # if skip train then load checkpoints
        if args.skip_train:
            model_conf["params"]["LOAD_CHECKPOINT"] = True
        # run the model

        run_model_safe(global_config, model_name, models,
                       try_num=global_config["TRY_NUM_MAX"], dataset=dataset, args=args)


def run_model_safe(
        global_config: GlobalConfigType,
        name: str,
        models,
        try_num: int,
        dataset: TTSVerbatimMappingDataset,
        args: argparse.Namespace) -> None:
    if try_num == 0:
        get_logger().critical(f"Failed to run model {name}")
        return
    try:

        get_logger().info(
            f"Running model {name}\n Try number \
                {global_config['TRY_NUM_MAX'] - try_num +1}\
                /{global_config['TRY_NUM_MAX']}")
        print(models)
        models[name].run_pipeline(args)

    except Exception as e:
        get_logger().error(
            f"Try number {global_config['TRY_NUM_MAX'] - try_num+1}\
                /{global_config['TRY_NUM_MAX']} {e} {e.__traceback__} ")
        run_model_safe(global_config, name, models, try_num - 1, dataset, args)

        raise e


def get_model_dict(
        global_config: GlobalConfigType,
        dataset: GenericDataset) -> dict:
    names = [("simple_model", SimpleModel),
             ("part_context_model", PartContextModel),
             ("d3st_model", D3STModel),
             ("xml_d3st_context_model", XMLD3STContextModel),
             ("mistral7B_model", Mistral7BModel)

             ]
    out = {}

    for name, model_class in names:
        if not global_config["models"] or name not in global_config["models"].keys():
            continue
        out[name] = model_class(
            global_config,
            name,
            get_local_config=get_local_config_curry_data_load(
                global_config["DATA_LOAD_LOC"],
                global_config["DATA_SAVE_PREPROCESS"],
                model_name=name),
            dataset=dataset,
        )
    return out


def argument_parsing():
    parser = argparse.ArgumentParser()
    parser.add_argument("--config_location", "-c", type=str,
                        default="run_config.yaml",
                        help='Specify the configuration file location')

    parser.add_argument("--debug_level", "-d", type=str, default="INFO",
                        help='Specify the debug level')

    parser.add_argument("--force_reload", "-fr",
                        help="Force reload data",
                        default=False
                        )
    parser.add_argument("--skip-train", "-st",
                        help="Skip training phase, if set then then \
                                then checkpoints will be loaded",
                        action="store_true")

    parser.add_argument("--skip-predict", "-sp",
                        help="Skip training phase and prediction\
                                and only eval",
                        action="store_true")

    parser.add_argument("--prediction-count",
                        "-pc",
                        type=int,
                        default=None,
                        help="Number of prediction batches to make")
    parser.add_argument("--reload-cache", "-rc", action="store_true")
    args = parser.parse_args()

    return args


def load_run_config(config_location="run_config.yaml"):
    get_logger().info("Loading run config...")
    try:
        run_config = yaml.load(
            open(config_location, "r"), Loader=yaml.FullLoader)
    except Exception as e:
        get_logger().critical(e)
        exit(1)

    run_config = Option(run_config)

    try:
        run_config = run_config.unwrap()
    except NoneValueExeption as e:
        get_logger().critical(e)
        exit(1)

    return run_config


if __name__ == "__main__":
    main()
