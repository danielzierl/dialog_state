from modules_.logging_ import get_logger
from t5s import T5
from models_.pipeline import run_pipeline_default_factory
from types_.global_config_type import GlobalConfigType
from types_.generic_dataset_type_ import GenericDataset
from typing import Callable
from modules_.tsv_writers.part_context_tsv_writer import tsv_writer, \
    get_text_repr
from modules_.predict_ import predict_json
from modules_.eval_ import evaluate


class Model:
    def __init__(
            self,
            global_config: GlobalConfigType,
            name: str,
            get_local_config: Callable,
            dataset: GenericDataset):

        get_logger().debug("creating model part context")

        # set default pipeline, its the first thing that calls on model run
        self.dataset = dataset
        self.global_config = global_config
        self.name = name

        # create the undelying model and set the train method
        get_logger().debug(get_local_config)
        self.model: T5 = T5(get_local_config(
            global_config["models"][name]["params"]))
        self.train = self.model.fine_tune
        self.predict = self.model.predict
        self.predict_json = predict_json
        self.evaluate = evaluate

        self.run_pipeline = run_pipeline_default_factory(
            self, tsv_writer, get_text_repr_func=get_text_repr)
