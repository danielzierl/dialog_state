from modules_.eval_ import evaluate
from peft import PeftModel
from modules_.predict_ import predict_json
import hashlib
from util_.minfuncs import data_set_state_to_rand_indices, predict_transform_d3st
from peft import LoraConfig, get_peft_model
from peft import prepare_model_for_kbit_training
from modules_.logging_ import get_logger
from models_.pipeline import run_pipeline_default_factory
from types_.global_config_type import GlobalConfigType
from types_.generic_dataset_type_ import GenericDataset
from typing import Callable
from modules_.tsv_writers.xml_writer import tsv_writer, \
    get_text_repr
from transformers import AutoModelForCausalLM, AutoTokenizer, BitsAndBytesConfig, DataCollatorForLanguageModeling, Trainer, TrainingArguments, StoppingCriteriaList
from datasets import load_dataset
import torch
from modules_.data_process_ import convert_to_tsv
import os
from accelerate import FullyShardedDataParallelPlugin, Accelerator
from util_.minfuncs import group_dialogs_by_id, dump_to_json

from torch.distributed.fsdp.api import FullOptimStateDictConfig, FullStateDictConfig
import json
import functools
from modules_.eval_ import evaluate
device = "cuda" if torch.cuda.is_available() else "cpu"


class Model:
    cached_input_embeddings = {}

    def load_cache(self):
        if not os.path.exists(os.path.join(self.global_config["CACHE_DIR"])):
            os.mkdir(os.path.join(self.global_config["CACHE_DIR"]))

        if not os.path.exists(os.path.join(self.global_config["CACHE_DIR"], "embeddings")):
            return {}

        with open(os.path.join(self.global_config["CACHE_DIR"], "embeddings"), "r") as f:
            self.cached_input_embeddings = json.load(f)

    def save_cache(self):
        if not os.path.exists(os.path.join(self.global_config["CACHE_DIR"])):
            os.mkdir(os.path.join(self.global_config["CACHE_DIR"]))

        with open(os.path.join(self.global_config["CACHE_DIR"], "embeddings"), "w") as f:
            json.dump(self.cached_input_embeddings, f)

    def __init__(
            self,
            global_config: GlobalConfigType,
            name: str,
            get_local_config: Callable,
            dataset: GenericDataset):

        get_logger().debug("creating model mistral7B")

        # set default pipeline, its the first thing that calls on model run
        self.dataset = dataset
        if dataset:
            self.dataset.data_train = \
                data_set_state_to_rand_indices(self.dataset.data_train)
            self.dataset.data_dev = \
                data_set_state_to_rand_indices(self.dataset.data_dev)
        self.global_config = global_config
        self.name = name
        self.params = global_config["models"][self.name]["params"]

        convert_to_tsv(
            os.path.join(global_config["DATA_SAVE_PREPROCESS"],
                         self.name, "tts_verbatim_train.tsv"),
            group_dialogs_by_id(dataset.data_train),
            tsv_writer
        )
        convert_to_tsv(
            os.path.join(global_config["DATA_SAVE_PREPROCESS"],
                         self.name, "tts_verbatim_dev.tsv"),
            group_dialogs_by_id(dataset.data_dev),
            tsv_writer
        )
        convert_to_tsv(
            os.path.join(global_config["DATA_SAVE_PREPROCESS"],
                         self.name, "tts_verbatim_dev_no_d3st.tsv"),
            group_dialogs_by_id(dataset.data_dev),
            functools.partial(tsv_writer, no_d3st=True)
        )
        data_dev_grouped = group_dialogs_by_id(dataset.data_dev)
        data_train_grouped = group_dialogs_by_id(dataset.data_train)
        dump_to_json(data_train_grouped, os.path.join(
            global_config["DATA_SAVE_PREPROCESS"], self.name, "tts_verbatim_train_grouped_by_id.json"))
        dump_to_json(data_dev_grouped, os.path.join(
            global_config["DATA_SAVE_PREPROCESS"], self.name, "tts_verbatim_dev_grouped_by_id.json"))
        dump_to_json(dataset.data_train, os.path.join(
            global_config["DATA_SAVE_PREPROCESS"], self.name, "tts_verbatim_train.json"))
        dump_to_json(dataset.data_dev, os.path.join(
            global_config["DATA_SAVE_PREPROCESS"], self.name, "tts_verbatim_dev.json"))

        self.dataset_compatible_transformers = load_dataset(
            "json", data_files={
                "train": os.path.join(
                    global_config["DATA_SAVE_PREPROCESS"], self.name, "tts_verbatim_train.json"),
                "dev": os.path.join(
                    global_config["DATA_SAVE_PREPROCESS"], self.name, "tts_verbatim_dev.json"),
            },
            cache_dir=global_config["CACHE_DIR"],

        )

        bnb_config = BitsAndBytesConfig(
            load_in_4bit=True,
            bnb_4bit_use_double_quant=True,
            bnb_4bit_quant_type="nf4",
            bnb_4bit_compute_dtype=torch.bfloat16
        )
        fsdp_plugin = FullyShardedDataParallelPlugin(
            state_dict_config=FullStateDictConfig(
                offload_to_cpu=True, rank0_only=False),
            optim_state_dict_config=FullOptimStateDictConfig(
                offload_to_cpu=True, rank0_only=False),
        )
        self.accelerator = Accelerator(fsdp_plugin=fsdp_plugin)

        # create the undelying model and set the train method
        self.tokenizer = AutoTokenizer.from_pretrained(
            global_config["models"][name]["params"]["BASE_MODEL"],
        )
        self.tokenizer.pad_token = self.tokenizer.eos_token

        self.model = AutoModelForCausalLM.from_pretrained(
            global_config["models"][name]["params"]["BASE_MODEL"],
            quantization_config=bnb_config,
            device_map="auto",
            trust_remote_code=True,
            use_auth_token=False
        )
        self.predict_json = predict_json
        self.evaluate = evaluate
        if self.global_config["models"][self.name]["params"]["LOAD_CHECKPOINT"]:
            self.load_checkpoint()

    def load_checkpoint(self):
        get_logger().info("Loading checkpoint")
        self.model = PeftModel.from_pretrained(
            self.model, "./checkpoints/d3st_mistral_model/mistral7B_model/checkpoint-130")

    def run_pipeline(self, args):
        if not args.skip_train:
            self.train()
        if not args.skip_predict:
            self.predict_json(
                from_file_json=os.path.join(
                    self.global_config["DATA_SAVE_PREPROCESS"], self.name, "tts_verbatim_dev_grouped_by_id.json"),
                to_file_tsv=os.path.join(
                    self.global_config["DATA_PREDICT_LOC"], self.name, "tts_verbatim_dev.pred.tsv"),
                model=self,
                num_batches=args.prediction_count,
                get_text_repr_func=get_text_repr,
                tsv_writer_func=tsv_writer,
                predict_output_transform_func=predict_transform_d3st
            )
        self.evaluate(
            pred_file=os.path.join(
                self.global_config["DATA_PREDICT_LOC"], self.name, "tts_verbatim_dev.pred.tsv"),
            gold_file=os.path.join(
                self.global_config["DATA_SAVE_PREPROCESS"], self.name, "tts_verbatim_dev_no_d3st.tsv"),

            pred_file_json=os.path.join(
                self.global_config["DATA_SAVE_PREPROCESS"], self.name,
                "tts_verbatim_dev_grouped_by_id.json"),
            eval_folder=os.path.join(
                self.global_config["DATA_EVAL_LOC"], self.name)

        )

    def train(self):
        self.model.gradient_checkpointing_enable()
        self.model = prepare_model_for_kbit_training(self.model)

        lora_config = LoraConfig(
            r=32,
            lora_alpha=2*32,
            target_modules=[
                "q_proj",
                "k_proj",
                "v_proj",
                "o_proj",
                "gate_proj",
                "up_proj",
                "down_proj",
                "lm_head",
            ],
            bias="none",
            lora_dropout=0.05,  # Optional dropout
            task_type="CAUSAL_LM",
        )
        self.model = get_peft_model(self.model, lora_config)
        print_trainable_parameters(self.model)
        # Apply the accelerator. You can comment this out to remove the accelerator.
        self.model = self.accelerator.prepare_model(self.model)

        def tokenize(data):
            input, output = get_text_repr(data)
            text = f"{input} {output}"
            cache_key = hashlib.md5(text.encode("utf-8")).hexdigest()
            if cache_key in self.cached_input_embeddings:
                return self.cached_input_embeddings[cache_key]
            result = self.tokenizer(
                text,
                truncation=True,
                max_length=self.params["INPUT_SIZE"],
                padding="max_length",
            )
            result["labels"] = result["input_ids"].copy()
            self.cached_input_embeddings[cache_key] = result
            return result
        self.dataset_compatible_transformers_embedings = self.dataset_compatible_transformers\
            .map(tokenize)

        trainer = Trainer(
            model=self.model,
            train_dataset=self.dataset_compatible_transformers_embedings["train"],
            args=TrainingArguments(
                output_dir=os.path.join(
                    self.params["MODEL_CHECKPOINT"], self.name),
                logging_strategy="epoch",
                warmup_steps=5,
                per_device_train_batch_size=self.params["BATCH_SIZE_TRAIN"],
                gradient_accumulation_steps=1,
                max_steps=self.params["STEPS_PER_EPOCH"] *
                self.params["EPOCHS"],
                learning_rate=3e-4,  # Want about 10x smaller than the Mistral learning rate
                logging_steps=50,
                bf16=False,
                optim="paged_adamw_8bit",
                logging_dir="./logs",        # Directory for storing logs
                save_strategy="steps",       # Save the model checkpoint every logging step
                save_steps=self.params["SAVE_CHECKPOINT_EVERY"] * \
                self.params["STEPS_PER_EPOCH"],
                report_to="wandb",

            ),
            data_collator=DataCollatorForLanguageModeling(
                self.tokenizer, mlm=False),
        )
        # silence the warnings. Please re-enable for inference!
        self.model.config.use_cache = False

        trainer.train()

    @torch.no_grad()
    def predict(self, batch_data_text: list[str]):

        self.model.eval()
        stop_sequence = "</state>"
        stop_sequence_enc: list = self.tokenizer.encode(stop_sequence)
        stop_sequence_enc.remove(1)

        def custom_stop_crit(input_ids: torch.LongTensor, score: torch.FloatTensor, **kwargs) -> bool:
            for stop_id, input_ids in zip(reversed(stop_sequence_enc), reversed(input_ids[0])):
                if stop_id != input_ids:
                    return False
            return True
        arr_decoded = []
        self.model.config.use_cache = True

        for data_text in batch_data_text:

            eval_prompt = f"{data_text} <state>"
            model_input = self.tokenizer(
                eval_prompt, return_tensors="pt").to("cuda")
            generated = self.model.generate(
                **model_input,
                max_new_tokens=self.params["OUTPUT_SIZE"],
                pad_token_id=2,
                stopping_criteria=StoppingCriteriaList([custom_stop_crit]),

            )
            decoded = self.tokenizer.decode(
                generated[0], skip_special_tokens=True,
            )

            decoded = decoded.replace(data_text, "")
            # decoded = "<state>1=itta benna; 2=yes </state>"
            get_logger().debug(f"Input: {data_text}")
            get_logger().debug(f"Decoded: {decoded}")
            arr_decoded.append(decoded)

        return arr_decoded


def print_trainable_parameters(model):
    """
    Prints the number of trainable parameters in the model.
    """
    trainable_params = 0
    all_param = 0
    for _, param in model.named_parameters():
        all_param += param.numel()
        if param.requires_grad:
            trainable_params += param.numel()
    print(
        f"trainable params: {trainable_params} || all params: {all_param} || trainable%: {100 * trainable_params / all_param}"
    )
