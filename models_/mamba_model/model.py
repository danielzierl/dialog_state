import torch

from transformers import AutoTokenizer, TrainingArguments
from datasets import load_dataset

model_name = "havenhq/mamba-chat"
tokenizer_name = "HuggingFaceH4/zephyr-7b-beta"


def train(train_dataset):

    model = MambaLMHeadModel.from_pretrained(
        model_name, dtype=torch.float16, device="cuda")

    tokenizer = AutoTokenizer.from_pretrained(tokenizer_name)
    tokenizer.eos_token = "<|endoftext|>"
    tokenizer.pad_token = tokenizer.eos_token

    def tokenize(data):
        text = f"{data['input']} state:{data['output']}"
        result = tokenizer(
            text,
            truncation=True,
            max_length=512,
            padding="max_length",
        )
        result["labels"] = result["input_ids"].copy()
        # result["labels"].
        return result
    dataset_train_embed = train_dataset.map(tokenize)

    trainer = MambaTrainer(
        model=model,
        train_dataset=dataset_train_embed,
        tokenizer=tokenizer,
        args=TrainingArguments(
            learning_rate=2.5e-3,
            num_train_epochs=1,
            max_steps=200,
            per_device_train_batch_size=2,
            gradient_accumulation_steps=1,
            optim="adamw_torch",
            output_dir="mamba-chat",
            logging_steps=50,
            save_steps=500,
            fp16=False,
        ),
    )

    trainer.train()


if __name__ == "__main__":
    import os
    import sys
    root_dir = os.path.abspath(".")
    sys.path.append(root_dir)
    from models_.mamba_model.mamba_trainer import MambaTrainer
    from lib_.mamba_ssm.models.mixer_seq_simple import MambaLMHeadModel
    train_dataset = load_dataset(
        'json', data_files='dataset.json', split='train')
    train(train_dataset)

else:
    from models_.mamba_model.mamba_trainer import MambaTrainer
    from lib_.mamba_ssm.models.mixer_seq_simple import MambaLMHeadModel
