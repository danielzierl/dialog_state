from modules_.logging_ import get_logger
from t5s import T5
from models_.pipeline import run_pipeline_default_factory
from types_.global_config_type import GlobalConfigType
from types_.generic_dataset_type_ import GenericDataset
from typing import Callable
from modules_.tsv_writers.xml_writer import tsv_writer, \
    get_text_repr
from modules_.predict_ import predict_json
from modules_.eval_ import evaluate
from util_.minfuncs import data_set_state_to_rand_indices, predict_transform_d3st, remap_fullslots_from_slot_map
from types_.dialogue_turn_type_ import DialogueTurnD3ST


class Model:
    def __init__(
            self,
            global_config: GlobalConfigType,
            name: str,
            get_local_config: Callable,
            dataset: GenericDataset):

        get_logger().debug("creating model d3st")

        # set default pipeline, its the first thing that calls on model run
        self.dataset = dataset
        if dataset:
            self.dataset.data_train = \
                data_set_state_to_rand_indices(self.dataset.data_train)
            self.dataset.data_dev = \
                data_set_state_to_rand_indices(self.dataset.data_dev)
        else:
            get_logger().warning("something is wrong, there is no dataset")
        self.global_config = global_config
        self.name = name

        # create the undelying model and set the train method
        get_logger().debug(get_local_config)
        self.model: T5 = T5(get_local_config(
            global_config["models"][name]["params"]))
        self.train = self.model.fine_tune
        self.predict = self.model.predict
        self.predict_json = predict_json
        self.evaluate = evaluate

        self.run_pipeline = run_pipeline_default_factory(
            self, tsv_writing_func=tsv_writer, get_text_repr_func=get_text_repr,
            predict_output_transform_func=predict_transform_d3st,
            generate_no_d3st_dev_tsv=True)
