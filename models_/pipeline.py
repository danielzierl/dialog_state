from modules_.logging_ import get_logger
from typing import Callable
import os
import pathlib
from types_.global_config_type import ModelParams
from types_.generic_dataset_type_ import GenericDataset
from types_.global_config_type import GlobalConfigType
from modules_.data_process_ import convert_to_tsv
from util_.minfuncs import group_dialogs_by_id, dump_to_json
from types_.generic_model_type_ import GenericModel
import argparse
import functools


def run_pipeline_default_factory(self: GenericModel,
                                 tsv_writing_func: Callable,
                                 generate_no_d3st_dev_tsv: bool = False,
                                 get_text_repr_func: Callable = None,
                                 predict_output_transform_func: Callable = None,
                                 ) -> Callable:
    dataset: GenericDataset = self.dataset
    config: GlobalConfigType = self.global_config

    def inner(args: argparse.Namespace) -> None:
        get_logger().debug("running pipeline")
        convert_to_tsv(
            os.path.join(config["DATA_SAVE_PREPROCESS"],
                         self.name, "tts_verbatim_train.tsv"),
            group_dialogs_by_id(dataset.data_train),
            tsv_writing_func
        )
        convert_to_tsv(
            os.path.join(config["DATA_SAVE_PREPROCESS"],
                         self.name, "tts_verbatim_dev.tsv"),
            group_dialogs_by_id(dataset.data_dev),
            tsv_writing_func
        )

        # also for dev eval make tsv without the d3st
        if (generate_no_d3st_dev_tsv):
            convert_to_tsv(
                os.path.join(config["DATA_SAVE_PREPROCESS"],
                             self.name, "tts_verbatim_dev_no_d3st.tsv"),
                group_dialogs_by_id(dataset.data_dev),
                functools.partial(tsv_writing_func, no_d3st=True)
            )

        data_dev_grouped = group_dialogs_by_id(dataset.data_dev)
        data_train_grouped = group_dialogs_by_id(dataset.data_train)
        dump_to_json(data_train_grouped, os.path.join(
            config["DATA_SAVE_PREPROCESS"], self.name, "tts_verbatim_train_grouped_by_id.json"))
        dump_to_json(data_dev_grouped, os.path.join(
            config["DATA_SAVE_PREPROCESS"], self.name, "tts_verbatim_dev_grouped_by_id.json"))

        if not args.skip_train:
            self.train()
        else:
            get_logger().info("Skipping training")

        if not args.skip_predict:
            self.predict_json(
                from_file_json=os.path.join(
                    config["DATA_SAVE_PREPROCESS"],
                    self.name,
                    "tts_verbatim_dev_grouped_by_id.json"
                ),
                to_file_tsv=os.path.join(
                    config["DATA_PREDICT_LOC"],
                    self.name,
                    "tts_verbatim_dev.pred.tsv"
                ),
                model=self,
                num_batches=args.prediction_count,
                get_text_repr_func=get_text_repr_func,
                tsv_writer_func=tsv_writing_func,
                predict_output_transform_func=predict_output_transform_func
            )
        else:
            get_logger().info("Skipping prediction")
        self.evaluate(
            pred_file=os.path.join(
                config["DATA_PREDICT_LOC"], self.name,
                "tts_verbatim_dev.pred.tsv"),
            gold_file=os.path.join(
                config["DATA_SAVE_PREPROCESS"], self.name,
                "tts_verbatim_dev_no_d3st.tsv" if generate_no_d3st_dev_tsv else "tts_verbatim_dev.tsv"),
            pred_file_json=os.path.join(
                config["DATA_SAVE_PREPROCESS"], self.name,
                "tts_verbatim_dev_grouped_by_id.json"),
            eval_folder=os.path.join(config["DATA_EVAL_LOC"], self.name)
        )

    return inner


def get_local_config_curry_data_load(data_load_loc: str, data_preprocess_loc: str, model_name: str) -> Callable:
    cwd = pathlib.Path().resolve()

    def get_local_config(params: ModelParams) -> dict:
        conf = {
            "tokenizer": {
                "spm": os.path.join(data_load_loc, "cc_all.32000/sentencepiece.model"),
            },
            "t5_model": {
                "pre_trained": params["PRETRAINED"],
                "save_checkpoint": params["MODEL_CHECKPOINT"],
                "save_checkpoint_every": params["SAVE_CHECKPOINT_EVERY"],
            },
            "dataset": {
                "train_tsv": os.path.join(cwd, data_preprocess_loc, model_name, "tts_verbatim_train.tsv"),
                "devel_tsv": os.path.join(cwd, data_preprocess_loc, model_name, "tts_verbatim_dev.tsv"),
                "loader": {
                    "input_size": params["INPUT_SIZE"],
                    "output_size": params["OUTPUT_SIZE"],
                    "min_batch_size": 1,
                },
            },
            "training": {
                "shared_trainable": True,
                "encoder_trainable": True,
                "n_epochs": params["EPOCHS"],
                "initial_epoch": 0,
                "steps_per_epoch": params["STEPS_PER_EPOCH"],
                "learning_rate": 0.001,
                "learning_rate_schedule": True,
            },
            "predict": {
                "batch_size": params["BATCH_SIZE_PREDICT"],
                "max_input_length": params["INPUT_SIZE"],
                "max_output_length": params["OUTPUT_SIZE"],
            }
        }
        if params["LOAD_CHECKPOINT"]:
            conf["t5_model"]["load_checkpoint"] = params["MODEL_CHECKPOINT"]

        return conf
    return get_local_config
