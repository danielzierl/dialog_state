import torch
from peft import PeftModel
from transformers import AutoTokenizer, AutoModelForCausalLM, BitsAndBytesConfig, AutoModelForSeq2SeqLM, Trainer, TrainingArguments, DataCollatorForLanguageModeling, StoppingCriteriaList
base_model_id = "mistralai/Mistral-7B-Instruct-v0.2"
bnb_config = BitsAndBytesConfig(
    load_in_4bit=True,
    bnb_4bit_use_double_quant=True,
    bnb_4bit_quant_type="nf4",
    bnb_4bit_compute_dtype=torch.bfloat16
)

base_model = AutoModelForCausalLM.from_pretrained(
    base_model_id,  # Mistral, same as before
    quantization_config=bnb_config,  # Same quantization config as before
    device_map="auto",
    trust_remote_code=True,
    use_auth_token=False
)
tokenizer = AutoTokenizer.from_pretrained(
    base_model_id, trust_remote_code=True)
tokenizer.pad_token = tokenizer.eos_token
ft_model = PeftModel.from_pretrained(
    base_model, "./checkpoints/d3st_mistral_model/mistral7B_model/checkpoint-130")


stop_sequence = "</state>"
stop_sequence_enc: list = tokenizer.encode(stop_sequence)
stop_sequence_enc.remove(1)


def custom_stop_crit(input_ids: torch.LongTensor, score: torch.FloatTensor, **kwargs) -> bool:
    for stop_id, input_ids in zip(reversed(stop_sequence_enc), reversed(input_ids[0])):
        if stop_id != input_ids:
            return False
    return True


while True:
    eval_prompt = input("Enter prompt: ")
    eval_prompt = f"{eval_prompt} <state>"
    model_input = tokenizer(eval_prompt, return_tensors="pt").to("cuda")
    ft_model.eval()
    with torch.no_grad():
        generated = ft_model.generate(
            **model_input,
            max_new_tokens=100,
            pad_token_id=2,
            stopping_criteria=StoppingCriteriaList([custom_stop_crit]),
        )
        decoded = tokenizer.decode(
            generated[0], skip_special_tokens=True,
        )
        print(decoded)
