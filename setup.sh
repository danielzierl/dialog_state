#! /bin/bash

if [ -d ".venv" ]; then
    echo "venv already exists"
else
    python3 -m venv .venv
fi

source .venv/bin/activate
pip install -r requirements.txt
python3 main.py >> logs.txt
