from types_.dialogue_turn_type_ import DialogueTurnD3ST
from util_.slots import slots
import json
import random
from tqdm import tqdm
from modules_.logging_ import get_logger


def dump_to_json(dataDict: dict, name: str):
    with open(name, "w", encoding="utf-8") as fw:
        json.dump(dataDict, fw)


def group_dialogs_by_id(dataList: dict):
    '''
     converts the list of dict of dialog metadata to a
     dict of dialog id to list of dialog metadata for specific dialog
    '''
    out = {}
    for d in dataList:
        if not d["dialog_id"] in out:
            out[d["dialog_id"]] = []
        out[d["dialog_id"]].append(d)

    return out


def clean_and_parse_fullslots(state: str):
    if not state:
        return [], []
    state = state.replace("state:", "")
    state = state.replace("<state>", "")
    state = state.replace("</state>", "")
    state = state.strip()
    state = state.split(";")
    state = [stat for stat in state if stat]
    state = [e.replace(";", "") for e in state]
    state = [e.strip().split("=") for e in state]
    for slot_arr in state:
        if len(slot_arr) < 2:
            return [], []
    return [e[0] for e in state], [e[1] for e in state]


def predict_transform_d3st(
        prediction: str,
        json_metadata: DialogueTurnD3ST):
    return remap_fullslots_from_slot_map(
        prediction, json_metadata["slot_map"])


def join_separate_name_value_slots(names, values):
    out = ""
    for name, value in zip(names, values):
        out += str(name) + "=" + str(value) + "; "
    return out


def remap_fullslots_from_slot_map(prediction, map):
    # print(f"{prediction=}, {map=}")
    names_indices, values = clean_and_parse_fullslots(prediction)
    print(f"{names_indices=}, {values=}")
    try:
        names = [map[name] for name in names_indices]
    except KeyError:
        get_logger().warning(
            f"Failed to map arbitrary slot name indices \
                    to descriptors {names_indices=}, \
                    {names=}, {values=}")
        return ''
    out = join_separate_name_value_slots(names, values)
    return out


def load_json_to_dict(file_name) -> dict:
    with open(file_name, 'r') as f:
        data = json.load(f)
    return data


def get_random_map_of_slots():
    slot_names = list(slots.keys())
    random.shuffle(slot_names)
    return ({ind: name for ind, name in enumerate(slot_names)},
            {name: ind for ind, name in enumerate(slot_names)})


def data_set_state_to_rand_indices(dataArr: dict):
    out = []
    for data in tqdm(dataArr):
        # generate map
        map_ind_to_name, map_name_to_ind = get_random_map_of_slots()

        # try to remap the name to embedding
        try:
            slot_names, slot_values = clean_and_parse_fullslots(
                data["user_state"])
            slot_names = [map_name_to_ind[name] for name in slot_names]

            data["user_state_non_mapped"] = data["user_state"]
            data["user_state"] = join_separate_name_value_slots(
                slot_names, slot_values)
            data["slot_map"] = map_ind_to_name
            data["descriptions"] = {}

        except KeyError:
            get_logger().warning("Failed to map state to arbitrary indices")
            get_logger().warning(data["user_state"])
        # for slot_name in slot_names:
        #     if slot_name not in slots.keys():
        #         continue
        #     data["descriptions"][slot_name] = slots[slot_name].description

        out.append(data)

    return out
