slots = {
    "train-arriveby": {
        "values": [],
        "description": "Time of train arrival at the station"
    },
    "train-day": {
        "values": ['monday', 'sunday', 'friday', 'wednesday', 'tuesday', 'saturday', 'thursday'],
        "description:": "Day of the week the train arrives"
    },
    "train-departure": {
        "values": [],
        "description": "Time of train departure from the station"
    },
    "train-destination": {
        "values": [],
        "description": "Where the train is going"
    },
    "train-leaveat": {
        "values": [],
        "description": "Time of train leaving the station"
    },
    "train-people": {
        "values": [],
        "description": "Number of people on the train"
    },
    "restaurant-area": {
        "values": [],
        "description": "Location of the restaurant"
    },
    "restaurant-day": {
        "values": ['monday', 'friday', 'tuesday', 'wednesday', 'thursday'],
        "description": "Day of the week when visiting restaurant"
    },
    "restaurant-food": {
        "values": [],
        "description": "What kind of food is served at the restaurant"
    },
    "restaurant-name": {
        "values": [],
        "description": ""
    },
    "restaurant-people": {
        "values": [],
        "description": ""
    },
    "restaurant-pricerange": {
        "values": ['expensive', 'moderate', 'cheap'],
        "description": ""
    },
    "restaurant-time": {
        "values": "",
        "description": ""
    },
    "taxi-arriveby": {
        "values": [],
        "description": ""
    },
    "taxi-departure": {
        "values": [],
        "description": ""
    },
    "taxi-destination": {
        "values": [],
        "description": ""
    },
    "taxi-leaveat": {
        "values": [],
        "description": ""
    },
    "hotel-area": {
        "values": ['east', 'dontcare', 'north', 'west', 'centre', 'south'],
        "description": ""
    },
    "hotel-day": {
        "values": ['monday', 'sunday', 'friday', 'tuesday', 'wednesday', 'saturday'],
        "description": ""
    },
    "hotel-internet": {
        "values": ['yes', 'no'],
        "description": ""
    },
    "hotel-name": {
        "values": [],
        "description": ""
    },
    "hotel-parking": {
        "values": ['no', 'dontcare', 'yes'],
        "description": ""
    },
    "hotel-people": {
        "values": [],
        "description": ""
    },
    "hotel-pricerange": {
        "values": ['dontcare', 'cheap', '$100', 'moderate', 'expensive'],
        "description": ""
    },
    "hotel-stars": {
        "values": ['0', '1', '2', '3', '4', '5'],
        "description": ""
    },
    "hotel-stay": {
        "values": [],
        "description": ""
    },
    "hotel-type": {
        "values": ['hotel', 'guesthouse'],
        "description": ""
    },
    "attraction-area": {
        "values": ['east', 'north', 'west', 'centre', 'south'],
        "description": ""
    },
    "attraction-name": {
        "values": [],
        "description": ""
    },
    "attraction-type": {
        "values": [],
        "description": ""
    },
    "hospital-department": {
        "values": [],
        "description": ""
    },
    "bus-leaveat": {
        "values": [],
        "description": ""
    },
    "bus-destination": {
        "values": [],
        "description": ""
    },
    "bus-departure": {
        "values": [],
        "description": ""
    },
    "bus-day": {
        "values": ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"],
        "description": ""
    },
}

possible_slot_value_dict = {
    'train-arriveby': ['5 pm', '7:15 pm', '10:15 am', '6:15 pm', '6 pm', '10:45 am', '12 pm', '10:30 am', '8:45 pm', '8:15 pm', '6:45 pm', '3:30 pm', '9 pm', '6:30 pm', '8:30 am', '4:30 pm', '5:45 pm', '9:45 pm', '8 am', '12:15 pm', '7 pm', '7:30 pm', '7:45 pm', '8:15 am', '3 pm', '5:15 pm', '9:15 am', '9:45 am', '4:15 pm', '1 pm', '2:30 pm', '8:45 am', '11:30 am', '11:45 am', '4:45 pm', '2 pm', '1:45 pm', '4 pm'],
    'train-departure': ['ely', 'bishops stortford', 'norwich', 'stevenage', 'leicester', 'london liverpool street', 'stansted airport', 'kings lynn', 'london kings cross', 'broxbourne', 'peterborough', 'cambridge'],
    'train-destination': ['ely', 'bishops stortford', 'norwich', 'stevenage', 'leicester', 'london liverpool street', 'birmingham new street', 'kings lynn', 'broxbourne', 'peterborough', 'london kings cross', 'stansted airport', 'london', 'cambridge', 'glastonbury'],
    'train-leaveat': ['9:39 am', '9:50 pm', '9:11 pm', '12 pm', '6:45 am', '2:45 pm', '8:15 pm', '9:29 am', '9 pm', '6:30 pm', '7 am', '7:21 am', '1:54 pm', '5:30 pm', '7 pm', '5:15 am', '8:15 am', '1532', '3:15 pm', '845', '12:30 pm', 'dontcare', '11:15 am', '4:15 pm', '11:30 am', '11:45 am', '4:45 pm', '7:40 pm', '1:45 pm', '7:16 pm', '5 pm', '12:45 pm', '5:45 am', '3:45 pm', '3:50 pm', '10:09 pm', '3:30 pm', '8:30 pm', '0 am', '5 am', '11 am', '5:45 pm', '9:45 pm', '8 am', '7:30 pm', '1:30 pm', '1:15 pm', '5:36 am', '5:16 am', '5:15 pm', '6:40 pm', '8:45 am', '2 pm', '9:30 pm', '10:15 am', '6:15 pm', '2:15 pm', '10:45 am', '8:45 pm', '6:36 pm', '6:45 pm', '7:54 am', '6:30 am', '3:11 pm', '9:30 am', '9 am', '1:29 pm', '9:15 am', '1 pm', '2:30 pm', '4 pm', 'morning', '5:54 am', '7:15 pm', '10 pm', '6 pm', '10:30 am', '10 am', '8:30 am', '9:01 pm', '9:15 pm', '4:30 pm', '1145', '12:15 pm', '8 pm', '7:45 pm', '3 pm', '3:54 pm', '9:45 am'],
    'restaurant-food': ['cuban', 'modern global', 'swedish', 'south indian', 'italian', 'jamaican', 'crossover', 'french', 'kosher', 'barbeque>modern european', 'thai and chinese', 'moroccan', 'polish', 'british', 'german', 'persian', 'eastern european', 'malaysian', 'corsica', 'cantonese', 'spanish', 'dontcare', 'korean', 'jamaican>chinese', 'italian|indian', 'halal', 'modern european', 'chinese', 'gastropub', 'welsh', 'vietnamese', 'australian', 'afghan', 'australian|indian', 'mediterranean', 'vegetarian', 'creative', 'north indian', 'american', 'traditional', 'modern american', 'indonesian', 'bistro', 'fusion', 'european', 'sushi', 'indian', 'austrian', 'indian|african', 'venetian', 'spanish|portuguese', 'caribbean', 'russian', 'the americas', 'brazilian|portuguese', 'molecular gastronomy', 'afternoon tea', 'south african', 'mexican', 'lebanese', 'north american', 'new zealand', 'north african', 'barbeque', 'scottish', 'hungarian', 'international', 'turkish', 'modern eclectic', 'japanese', 'unusual', 'sri lankan', 'north american>indian', 'romanian', 'african', 'kosher|british', 'greek', 'catalan', 'seafood', 'english', 'asian oriental', 'latin american', 'world', 'panasian', 'northern european', 'scandinavian', 'belgian', 'canapes', 'steakhouse', 'thai', 'asian', 'eritrean', 'brazilian', 'basque', 'caribbean>indian', 'middle eastern', 'swiss', 'tuscan', 'portugese', 'traditional american', 'danish', 'christmas', 'irish', 'polynesian', 'singaporean', 'chinese|mexican', 'light bites'],
    'restaurant-name': ['rajmahal', 'the gandhi', 'prezzo', 'grafton hotel restaurant', 'royal spice', 'restaurant one seven', 'zizzi cambridge', 'golden curry', 'wagamama', 'shanghai family restaurant', 'mahal of cambridge', 'gardenia', 'yippee noodle bar', 'tang chinese', 'primavera', 'pizza hut fenditton', 'ashley hotel', 'slug and lettuce', 'dontcare', 'peking restaurant', 'city stop restaurant', 'shiraz', 'midsummer house restaurant', 'cote', 'tandoori palace', 'saigon city', 'maharajah tandoori restaurant', 'panahar', 'la margherita', 'india house', 'backstreet bistro', 'adden', 'good luck', 'efes restaurant', 'michaelhouse cafe', 'yu garden', 'stazione restaurant and coffee bar', 'sala thong', 'the lucky star', 'jinling noodle bar', 'galleria', 'hk fusion', 'pizza hut cherry hinton', 'restaurant alimentum', 'nus', 'curry garden', 'hobsons house', 'meze bar', 'saffron brasserie', 'autumn house', 'saint johns chop house', 'the missing sock', 'darrys cookhouse and wine shop', 'clowns cafe', 'bangkok city', 'thanh binh', 'hakka', 'la tasca', 'don pasquale pizzeria', 'meghna', 'pizza hut city centre', 'nirala', 'dojo noodle bar', 'cityr', 'sitar tandoori', 'the gardenia', 'cocum', 'golden house', 'graffiti', 'loch fyne', 'alex', 'the slug and lettuce', 'da vinci pizzeria', 'copper kettle', 'chiquito restaurant bar', 'j restaurant', 'la mimosa', 'cafe uno', 'eraina', 'nusha', 'travellers rest', 'the river bar steakhouse and grill', 'ugly duckling', 'the hotpot', 'ask', 'curry queen', 'gourmet burger kitchen', 'rice house', 'lovel', 'pizza hut', 'fitzbillies restaurant', 'la raza', 'frankie and bennys', 'anatolia', 'bloomsbury restaurant', 'nandos', 'cotto', 'charlie chan', 'curry king', 'curry prince', 'cow pizza kitchen and bar', 'cambridge lodge restaurant', 'riverside brasserie', 'ali baba', 'kohinoor', 'tandoori', 'the oak bistro', 'rice boat', 'bedouin', 'lan hong house', 'taj tandoori', 'the varsity restaurant', 'the cow pizza kitchen and bar', 'parkside pools', 'golden wok', 'de luca cucina and bar', 'restaurant two two', 'nandos city centre', 'sesame restaurant and bar', 'pizza express', 'cambridge chop house', 'oak bistro', 'efes', 'kymmoy', 'hotel du vin and bistro', 'little seoul', 'pipasha restaurant', 'royal standard'],
    'restaurant-time': ['5 pm', '7:15 pm', '12:45 pm', '6:15 pm', '10:15 am', '6 pm', '2:15 pm', '10:45 am', '12 pm', '3:45 pm', '10:30 am', '10 am', '1430', '1330', '2:45 pm', '17:00|16:00', '8:45 pm', '8:15 pm', '6:45 pm', '9', '3:30 pm', '8:30 pm', '9 pm', '6:30 am', '6:30 pm', '15:30|16:30', '4:30 pm', '11 am', '5:45 pm', '5:30 pm', '12:15 pm', '7:30 pm', '7 pm', '4pm', '8 pm', '1:30 pm', '7:45 pm', '1:15 pm', '11:30|12:30', '3 pm', '3:15 pm', '9 am', '5:15 pm', '9:15 am', '3 am', '12:30 pm', '9:45 am', 'dontcare', '11:15 am', '1 pm', '4:15 pm', '8pm', '2:30 pm', '11:30 am', '11:45 am', '4:45 pm', '2 pm', '1:45 pm', '4 pm'],
    'taxi-arriveby': ['5 pm', '10 pm', '6:15 pm', '6 pm', '12 pm', '11:30 pm', '1 am', '9 pm', '11 am', '7:30 pm', '7 pm', '7:45 pm', '3 pm', '9:15 am', '9:45 am', '1 pm', '11:30 am', '11:45 am', '4:45 pm', '4 pm'],
    'taxi-departure': ['el shaddia guesthouse', 'broughton house gallery', 'hughes hall', 'emmanuel college', 'acorn guest house', 'tang chinese', 'ashley hotel', 'riverboat georgina', 'peking restaurant', 'cambridge', 'saigon city', 'maharajah tandoori restaurant', 'panahar', 'university arms hotel', 'india house', 'kirkwood house', 'the lucky star', 'jinling noodle bar', 'avalon', 'alpha-milton guest house', 'carolina bed and breakfast', 'pizza hut cherry hinton', 'holy trinity church', 'christ college', 'hobsons house', 'cambridge towninfo centre', 'a and b guest house', 'saffron brasserie', 'autumn house', 'clowns cafe', 'byard art', 'bangkok city', 'thanh binh', 'ambridge', 'wandlebury country park', 'archway house', 'dojo noodle bar', 'the gardenia', 'cocum', 'funky fun house', 'sheeps green and lammas land park', 'rosas bed and breakfast', 'graffiti', 'whipple museum of the history of science', 'loch fyne', 'bridge guest house', 'cambridge arts theatre', 'queens college', 'fitzwilliam museum', 'chiquito restaurant bar', 'club salsa', 'cambridge university botanic gardens', 'sheeps green and lammas land park fen causeway', 'lynne strover gallery', 'kings college', 'ugly duckling', 'finches bed and breakfast', 'the junction', 'clare college', 'huntingdon marriott hotel', 'la raza', 'frankie and bennys', 'hamilton lodge', 'cambridge train station', 'curry king', 'worth house', 'cambridge lodge restaurant', 'riverside brasserie', 'ali baba', 'kohinoor', 'the oak bistro', 'parkside police station', 'lan hong house', 'saint catharines college', 'alexander bed and breakfast', 'leverton house', 'nandos city centre', 'museum of archaelogy and anthropology', 'broxbourne train station', 'the man on the moon', 'warkworth house'],
    'taxi-destination': ['el shaddia guesthouse', 'scott polar museum', 'broughton house gallery', 'rajmahal', 'the gandhi', 'prezzo', 'grafton hotel restaurant', 'camboats', 'royal spice', 'gallery at twelve a high street', 'restaurant one seven', 'zizzi cambridge', 'saint barnabas press gallery', 'downing college', 'golden curry', 'hughes hall', 'wagamama', 'shanghai family restaurant', 'castle galleries', 'birmingham new street train station', 'acorn guest house', 'mahal of cambridge', 'cherry hinton village centre', 'ely train station', 'tang chinese', 'yippee noodle bar', 'home from home', 'adc theatre', 'churchills college', 'cherry hinton hall and grounds', 'primavera', 'pizza hut fenditton', 'limehouse', 'ashley hotel', 'cambridge county fair next to the city tourist museum', 'riverboat georgina', 'city stop restaurant', 'peking restaurant', 'midsummer house restaurant', 'shiraz', 'soul tree nightclub', 'all saints church', 'arbury lodge guesthouse', 'tandoori palace', 'cote', 'cambridge', 'abbey pool and astroturf pitch', 'old schools', 'saigon city', 'cherry hinton water play', 'maharajah tandoori restaurant', 'the regent street city center', 'panahar', 'university arms hotel', 'la margherita', 'india house', 'backstreet bistro', 'mumford theatre', 'good luck', 'efes restaurant', 'michaelhouse cafe', 'museum of classical archaeology', 'yu garden', 'stazione restaurant and coffee bar', 'broxbourne train station', 'the lucky star', 'avalon', 'jinling noodle bar', 'london liverpool street train station', 'alpha-milton guest house', 'galleria', 'carolina bed and breakfast', 'regency gallery', 'scudamores punting co', 'nil', 'hk fusion', 'pizza hut cherry hinton', 'pembroke college', 'aylesbray lodge guest house', 'restaurant alimentum', 'holy trinity church', 'cambridge museum of technology', 'saint johns college', 'cineworld cinema', 'christ college', 'leicester train station', 'curry garden', 'hobsons house', 'lovell lodge', 'a and b guest house', 'meze bar', 'saffron brasserie', 'city centre north b and b', 'saint johns chop house', 'the missing sock', 'darrys cookhouse and wine shop', 'lensfield hotel', 'byard art', 'stansted airport train station', 'clowns cafe', 'bangkok city', 'hakka', 'thanh binh', 'la tasca', 'don pasquale pizzeria', 'meghna', 'pizza hut city centre', 'wandlebury country park', 'cambridge book and print gallery', 'archway house', 'nirala', 'dojo noodle bar', 'cityroomz', 'tenpin', 'ballare', 'sitar tandoori', 'milton country park', 'addenbrookes hospital', 'gonville and caius college', 'cambridge belfry', 'the gardenia', 'finders corner newmarket road', 'cocum', 'funky fun house', 'cambridge contemporary art', 'golden house', 'rosas bed and breakfast', 'graffiti', 'loch fyne', 'whipple museum of the history of science', 'bridge guest house', 'little saint marys church', 'the slug and lettuce', 'allenbell', 'kambar', 'cambridge arts theatre', 'queens college', 'jesus green outdoor pool', 'fitzwilliam museum', 'da vinci pizzeria', 'gonville hotel', 'copper kettle', 'chiquito restaurant bar', 'club salsa', 'j restaurant', 'express by holiday inn cambridge', 'cambridge university botanic gardens', 'cafe uno', 'corpus christi', 'la mimosa', 'eraina', 'the cambridge corn exchange', 'sheeps green and lammas land park fen causeway', 'nusha', 'travellers rest', 'lynne strover gallery', 'the river bar steakhouse and grill', 'ugly duckling', 'kings college', 'london kings cross train station', 'great saint marys church', 'station road', 'ask', 'the hotpot', 'cambridge punter', 'finches bed and breakfast', 'gourmet burger kitchen', 'rice house', 'the junction', 'sidney sussex college', 'cambridge artworks', 'huntingdon marriott hotel', 'trinity college', 'fitzbillies restaurant', 'la raza', 'frankie and bennys', 'anatolia', 'jesus college', 'cambridge train station', 'hamilton lodge', 'saint barnabas', 'nandos', 'bloomsbury restaurant', 'whale of a time', 'williams art and antiques', 'cotto', 'charlie chan', 'curry king', 'curry prince', 'cow pizza kitchen and bar', 'worth house', 'cambridge lodge restaurant', 'vue cinema', 'riverside brasserie', 'ali baba', 'kohinoor', 'magdalene college', 'the oak bistro', 'rice boat', 'bedouin', 'parkside police station', 'hakka|acorn guest house', 'lan hong house', 'clare hall', 'stevenage train station', 'taj tandoori', 'alexander bed and breakfast', 'the varsity restaurant', 'cambridge and county folk museum', 'parkside pools', 'golden wok', 'de luca cucina and bar', 'kettles yard', 'restaurant two two', 'nandos city centre', 'pizza express', 'sesame restaurant and bar', 'cambridge chop house', 'kymmoy', 'hotel du vin and bistro', 'little seoul', 'the place', 'pipasha restaurant', 'museum of archaelogy and anthropology', 'royal standard', 'the man on the moon'],
    'taxi-leaveat': ['3:20 am', '12 pm', '11:30 pm', '6:45 am', '2:45 pm', '8:15 pm', '10:30 pm', '9 pm', '6:30 pm', '7 am', '5:30 pm', '7 pm', '3:25 pm', '5:15 am', '8:15 am', '3:15 pm', '5:30 am', '12:30 pm', 'dontcare', '11:15 am', '4:15 pm', '11:30 am', '11:45 am', '4:45 pm', '1:45 pm', '5 pm', '0:45 am', '12:45 pm', '5:45 am', '7:45 am', '3:45 pm', '3:30 pm', '4:15 am', '1:45 am', '2:15 am', '8:30 pm', '5 am', 'after 11:45 am', '11 am', '5:45 pm', '9:45 pm', '8 am', '7:30 pm', '1:30 pm', '11:45 pm', '7:30 am', '1:15 pm', '5:15 pm', '2:30 am', '8:45 am', '2 pm', '6:15 am', '9:30 pm', '10:15 am', '6:15 pm', '3:30 am', '2:15 pm', '10:45 am', '11:15 pm', '8:45 pm', '4:45 am', '1 am', '6:45 pm', '4:30 am', '6:30 am', '9:30 am', '10:50 am', '4 am', '1:30 am', '9 am', '9:15 am', '1 pm', 'thursday', '2:30 pm', '3:45 am', '4 pm', '9:25 pm', '7:15 pm', '10 pm', '1:15 am', '6 pm', '3:15 am', '2 am', '10:30 am', '10 am', '11 pm', '8:30 am', '9:15 pm', '4:30 pm', '6 am', '12:15 pm', '2:45 am', '8 pm', '7:45 pm', '7:15 am', '3 pm', '10:15 pm', '3 am', 'after 2:30 am', '9:45 am', '10:45 pm'],
    'hotel-name': ['el shaddia guesthouse', 'the ashley hotel|lovell lodge', 'yes', 'huntingdon marriott hotel', 'university arms hotel', 'cambridge belfry', 'hamilton lodge', 'huntingdon marriott hotel|university arms hotel', 'rosas bed and breakfast', 'sou', 'bridge guest house', 'kirkwood house', 'allenbell', 'avalon', 'huntingdon marriott hotel|cambridge belfry', 'alpha-milton guest house', 'carolina bed and breakfast', 'worth house', 'gonville hotel', 'aylesbray lodge guest house|rosas bed and breakfast', 'aylesbray lodge guest house', 'alexander|el shaddai|gonville hotel|university arms hotl', 'whale', 'acorn guest house', 'the alpha-milton|the hamilton lodge', 'the acorn guest house', 'no', 'ashley hotel|lovell lodge|cityroomz', 'hobsons house', 'lovell lodge', 'a and b guest house', 'home from home', 'express by holiday inn cambridge', 'city centre north b and b', 'autumn house', 'alexander bed and breakfast', 'lensfield hotel', 'city centre north b and b|el shaddai', 'limehouse', 'allenbell|autumn house|leverton house', 'cherr', 'the allenbell|autumn house|leverton house', 'ashley hotel', 'leverton house', 'north b and b', 'finches bed and breakfast', 'holiday inn', 'dontcare', 'alexander bed and breakfast|el shaddai', 'arbury lodge guesthouse', 'ashley hotel|lovell lodge', 'allenbell|alexander bed and breakfast', 'archway house', 'sleeperz', 'city centre b & b|el shaddai', 'cityroomz', 'warkworth house'],
    'hospital-department': ['paediatric day unit', 'cardiology and coronary care unit', 'clinical decisions unit', 'urology', 'respiratory medicine', 'neurosciences critical care unit', 'hepatology', 'medicine for the elderly', 'neonatal unit', 'diabetes and endocrinology', 'haematology', 'neurology neurosurgery', 'trauma high dependency unit', 'neurology', 'transplant high dependency unit', 'neurosciences', 'trauma and orthopaedics', 'acute medicine for the elderly', 'oral and maxillofacial surgery and ent', 'transitional care', 'surgery', 'antenatal', 'emergency department', 'paediatric clinic', 'medical decisions unit', 'clinical research facility', 'haematology day unit', 'inpatient occupational therapy', 'cardiology', 'gynaecology', 'gastroenterology', 'plastic and vascular surgery plastics', 'oncology', 'intermediate dependancy area', 'infusion services', 'childrens oncology and haematology', 'cambridge eye unit', 'infectious diseases', 'teenage cancer trust unit', 'haematology and haematological oncology', 'dontcare', 'hepatobillary and gastrointestinal surgery regional referral centre', 'paediatric intensive care unit', 'coronary care unit', 'childrens surgical and medicine', 'john farman intensive care unit', 'psychiatry', 'acute medical assessment unit'],
    'attraction-name': ['scott polar museum', 'broughton house gallery', 'the fez club', 'camboats', 'gallery at twelve a high street', 'saint barnabas press gallery', 'downing college', 'hughes hall', 'emmanuel college', 'castle galleries', 'cherry hinton village centre', 'churchills college', 'adc theatre', 'cherry hinton hall and grounds', 'primavera', 'boat', 'riverboat georgina', 'dontcare', 'cinema cinema', 'soul tree nightclub', 'all saints church', 'abbey pool and astroturf pitch', 'old schools', 'cherry hinton water play', 'museum', 'peoples portraits exhibition at girton college', 'mumford theatre', 'museum of classical archaeology', 'scudamores punting co', 'regency gallery', 'pembroke college', 'holy trinity church', 'cambridge museum of technology', 'saint johns college', 'cineworld cinema', 'christ college', 'byard art', 'wandlebury country park', 'cambridge book and print gallery', 'ballare', 'tenpin', 'milton country park', 'gonville and caius college', 'funky fun house', 'ruskin gallery', 'cambridge contemporary art', 'whipple museum of the history of science', 'little saint marys church', 'kambar', 'cambridge arts theatre', 'queens college', 'jesus green outdoor pool', 'college', 'fitzwilliam museum', 'club salsa', 'cafe jello gallery', 'cambridge university botanic gardens', 'sheeps green and lammas land park fen causeway', 'corpus christi', 'the cambridge corn exchange', 'lynne strover gallery', 'nusha', 'kings college', 'kings hedges learner pool', 'great saint marys church', 'cambridge punter', 'the junction', 'sidney sussex college', 'clare college', 'cambridge artworks', 'trinity college', 'jesus college', 'whale of a time', 'williams art and antiques', 'worth house', 'vue cinema', 'magdalene college', 'clare hall', 'saint catharines college', 'cambridge and county folk museum', 'parkside pools', 'kettles yard', 'the place', 'museum of archaelogy and anthropology', 'the man on the moon']



}
