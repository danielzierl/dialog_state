from peft import LoraConfig
from peft.mapping import get_peft_model
from peft.utils.other import prepare_model_for_kbit_training
from transformers import AutoTokenizer, AutoModelForCausalLM, BitsAndBytesConfig, AutoModelForSeq2SeqLM, Trainer, TrainingArguments, DataCollatorForLanguageModeling
import torch
from datasets import load_dataset
from accelerate import FullyShardedDataParallelPlugin, Accelerator
from torch.distributed.fsdp.api import FullOptimStateDictConfig, FullStateDictConfig

fsdp_plugin = FullyShardedDataParallelPlugin(
    state_dict_config=FullStateDictConfig(
        offload_to_cpu=True, rank0_only=False),
    optim_state_dict_config=FullOptimStateDictConfig(
        offload_to_cpu=True, rank0_only=False),
)
accelerator = Accelerator(fsdp_plugin=fsdp_plugin)


train_dataset = load_dataset('json', data_files='dataset.json', split='train')

print(train_dataset)

base_model_id = "mistralai/Mistral-7B-Instruct-v0.2"
bnb_config = BitsAndBytesConfig(
    load_in_4bit=True,
    bnb_4bit_use_double_quant=True,
    bnb_4bit_quant_type="nf4",
    bnb_4bit_compute_dtype=torch.float16
)
model = AutoModelForCausalLM.from_pretrained(
    base_model_id,
    quantization_config=bnb_config
)

tokenizer = AutoTokenizer.from_pretrained(
    base_model_id,
    model_max_length=512,
    padding_side="left",
    add_eos_token=True
)
tokenizer.pad_token = tokenizer.eos_token


def tokenize(data):
    text = f"{data['input']} state:{data['output']}"
    result = tokenizer(
        text,
        truncation=True,
        max_length=512,
        padding="max_length",
    )
    result["labels"] = result["input_ids"].copy()
    return result


tokenized_train_dataset = train_dataset.map(tokenize)
print(tokenized_train_dataset[0])
# tokenized_val_dataset = train_dataset.map(tokenize)


model.gradient_checkpointing_enable()
model = prepare_model_for_kbit_training(model)


def print_trainable_parameters(model):
    """
    Prints the number of trainable parameters in the model.
    """
    trainable_params = 0
    all_param = 0
    for _, param in model.named_parameters():
        all_param += param.numel()
        if param.requires_grad:
            trainable_params += param.numel()
    print(
        f"trainable params: {trainable_params} || all params: {all_param} || trainable%: {100 * trainable_params / all_param}"
    )


lora_config = LoraConfig(
    r=4,
    lora_alpha=8,
    target_modules=[
        "q_proj",
        "k_proj",
        "v_proj",
        "o_proj",
        "gate_proj",
        "up_proj",
        "down_proj",
        "lm_head",
    ],
    bias="none",
    lora_dropout=0.05,  # Optional dropout
    task_type="CAUSAL_LM",
)
model = get_peft_model(model, lora_config)
print_trainable_parameters(model)
# Apply the accelerator. You can comment this out to remove the accelerator.
model = accelerator.prepare_model(model)
trainer = Trainer(
    model=model,
    train_dataset=tokenized_train_dataset,
    args=TrainingArguments(
        output_dir="./results2",
        warmup_steps=5,
        per_device_train_batch_size=1,
        gradient_accumulation_steps=1,
        max_steps=50,
        learning_rate=3e-4,  # Want about 10x smaller than the Mistral learning rate
        logging_steps=50,
        bf16=False,
        optim="paged_adamw_8bit",
        logging_dir="./logs",        # Directory for storing logs
        save_strategy="steps",       # Save the model checkpoint every logging step
        save_steps=50,                # Save checkpoints every 50 steps
    ),
    data_collator=DataCollatorForLanguageModeling(
        tokenizer, mlm=False),
)
# silence the warnings. Please re-enable for inference!
model.config.use_cache = False

trainer.train()
stop_sequence = "</OUTPUT>"
stop_sequence_enc = tokenizer.encode(stop_sequence)[0]
while True:
    eval_prompt = input("Enter prompt: ")
    eval_prompt = f"<INPUT>{eval_prompt}</INPUT>"
    model_input = tokenizer(eval_prompt, return_tensors="pt").to("cuda")
    model.eval()
    with torch.no_grad():
        print(
            tokenizer.decode(
                model.generate(
                    **model_input,
                    max_new_tokens=256,
                    pad_token_id=2)[0],
                skip_special_tokens=True, ))
